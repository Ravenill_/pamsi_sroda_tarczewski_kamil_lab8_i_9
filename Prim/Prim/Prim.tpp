#include "QueueEdge.h"

template<typename Type>
void PrimAlghoritm(GraphArray<Type>& G, GraphArray<Type>& temp)
{
	int amount_ver = G.amountOfVer();
	int amount_edg = G.amountOfEdg();
	
	int iterator = 0;
	QueueEdge<Type> queue(amount_edg);
	Edge<Type> temp_edge;

	bool * visited = new bool[amount_ver];
	for (int i = 0; i < amount_ver; i++)
		visited[i] = 0;

	visited[iterator] = 1;

	temp.insertVertex(G.getVertex(iterator));
	visited[iterator] = 1;
	for (int j = 0; j < amount_ver; j++)
		if (G.getMatrix(iterator)[j] != nullptr)
			queue.push(*G.getMatrix(iterator)[j]);

	for (int i = 0; i < amount_ver - 1; i++)
	{
		do
		{
			temp_edge = queue.front();
			queue.pop();
			if (visited[(*temp_edge.getA()).getKey()] == 1)
				if (visited[(*temp_edge.getB()).getKey()] == 1)
					continue;
				else
				{
					temp.insertEdge(temp_edge);
					iterator = (*temp_edge.getB()).getKey();
				}
			else if (visited[(*temp_edge.getB()).getKey()] == 1)
				if (visited[(*temp_edge.getA()).getKey()] == 1)
					continue;
				else
				{
					temp.insertEdge(temp_edge);
					iterator = (*temp_edge.getA()).getKey();
				}
		} while (visited[iterator] != 0);

		temp.insertVertex(G.getVertex(iterator));
		visited[iterator] = 1;
		for (int j = 0; j < amount_ver; j++)
			if (G.getMatrix(iterator)[j] != nullptr)
				queue.push(*G.getMatrix(iterator)[j]);
	}

}

template<typename Type>
void PrimAlghoritm(GraphList<Type>& G, GraphList<Type>& temp)
{
	int amount_ver = G.amountOfVer();
	int amount_edg = G.amountOfEdg();

	int iterator = 0;
	QueueEdge<Type> queue(amount_edg);
	Edge<Type> temp_edge;

	bool * visited = new bool[amount_ver];
	for (int i = 0; i < amount_ver; i++)
		visited[i] = 0;

	visited[iterator] = 1;

	temp.insertVertex(G.getVertex(iterator));
	visited[iterator] = 1;
	for (int j = 0; j < G.getList(iterator).rozmiar(); j++)
		queue.push(*G.getList(iterator).wezDane(j));

	for (int i = 0; i < amount_ver - 1; i++)
	{
		do
		{
			temp_edge = queue.front();
			queue.pop();
			if (visited[(*temp_edge.getA()).getKey()] == 1)
				if (visited[(*temp_edge.getB()).getKey()] == 1)
					continue;
				else
				{
					temp.insertEdge(temp_edge);
					iterator = (*temp_edge.getB()).getKey();
				}
			else if (visited[(*temp_edge.getB()).getKey()] == 1)
				if (visited[(*temp_edge.getA()).getKey()] == 1)
					continue;
				else
				{
					temp.insertEdge(temp_edge);
					iterator = (*temp_edge.getA()).getKey();
				}
		} while (visited[iterator] != 0);

		temp.insertVertex(G.getVertex(iterator));
		visited[iterator] = 1;
		for (int j = 0; j < G.getList(iterator).rozmiar(); j++)
			queue.push(*G.getList(iterator).wezDane(j));
	}

}
