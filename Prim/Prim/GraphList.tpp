#include <iostream>

template<typename Type>
GraphList<Type>::GraphList()
{

}

template<typename Type>
GraphList<Type>::GraphList(int v, int e)
{
	edg = e;
	ver = v;
	vertex_id = 0;
	edge_id = 0;

	_vertex = new Vertex<Type> [ver];
	_edge = new Edge<Type> [edg];
	list = new dList<Edge<Type>*>[ver];
}

template<typename Type>
GraphList<Type>::~GraphList()
{
	delete[] _edge;
	delete[] _vertex;
	delete[] list;
}

template<typename Type>
Vertex<Type>* GraphList<Type>::endVerticles(Edge<Type> edge)
{
	return NULL;
}

template<typename Type>
Vertex<Type> GraphList<Type>::opposite(Vertex<Type> vertex, Vertex<Type> middle)
{
	return Vertex<Type>();
}

template<typename Type>
bool GraphList<Type>::areAdjacent(Vertex<Type> vertex1, Vertex<Type> vertex2)
{
	return false;
}

template<typename Type>
void GraphList<Type>::replace(Vertex<Type> vertex1, Vertex<Type> vertex2)
{

}

template<typename Type>
void GraphList<Type>::replace(Edge<Type> edge1, Edge<Type> edge2)
{

}

template<typename Type>
void GraphList<Type>::insertVertex(Type obj)
{
	if (vertex_id <= ver)
	{
		Vertex<Type> *temp = new Vertex<Type>;
		(*temp).setData(obj);
		(*temp).setKey(vertex_id);

		_vertex[vertex_id++] = *temp;
	}
}

template<typename Type>
void GraphList<Type>::insertVertex(Vertex<Type> vertex)
{
	if (vertex_id <= ver)
	{
		_vertex[vertex_id++] = vertex;
	}
}

template<typename Type>
void GraphList<Type>::insertEdge(Vertex<Type>* vertex1, Vertex<Type>* vertex2, Type obj)
{
	Edge<Type> *temp = new Edge<Type>;
	(*temp).setA(vertex1);
	(*temp).setB(vertex2);
	(*temp).setData(obj);

	if (1)
	{
		_edge[edge_id++] = *temp;
		list[(*vertex1).getKey()].wstawPierwszy(temp);
		list[(*vertex2).getKey()].wstawPierwszy(temp);
	}
	else
		delete temp;
}

template<typename Type>
void GraphList<Type>::insertEdge(Edge<Type> e)
{
	_edge[edge_id++] = e;
	list[(*e.getA()).getKey()].wstawPierwszy(&e);
	list[(*e.getB()).getKey()].wstawPierwszy(&e);
}

template<typename Type>
void GraphList<Type>::removeVertex(Vertex<Type> vertex)
{

}

template<typename Type>
void GraphList<Type>::removeEdge(Edge<Type> edge)
{

}

template<typename Type>
void GraphList<Type>::incidentEdges(Vertex<Type> vertex)
{

}

template<typename Type>
void GraphList<Type>::vertices()
{
	for (int i = 0; i < ver; i++)
		std::cout << _vertex[i].getData() << ":" << _vertex[i].getKey() <<  "  ";
	std::cout << "\n";
}

template<typename Type>
void GraphList<Type>::edges()
{
	for (int i = 0; i < edg; i++)
		std::cout << _edge[i].getData() << "  ";
	std::cout << "\n";
}

template<typename Type>
Vertex<Type> GraphList<Type>::searchVertex(int id)
{
	for (int i = 0; i < ver; i++)
	{
		if (_vertex[i].getKey() == id)
			return _vertex[i];
	}
	return Vertex<Type>();
}

template<typename Type>
void GraphList<Type>::insertEdge(int vertex1_id, int vertex2_id, Type obj)
{
	insertEdge(&_vertex[vertex1_id], &_vertex[vertex2_id], obj);
}

template<typename Type>
void GraphList<Type>::print()
{
	for (int i = 0; i < ver; i++)
	{
		for (int j = 0; j < list[i].rozmiar(); j++)
		{
			std::cout << (*list[i].wezDane(j)).getData() << "  ";
		}
		std::cout << "\n";
	}
}

template <typename Type>
void GraphList<Type>::makeMeGraphGizz(int ro)
{

}

