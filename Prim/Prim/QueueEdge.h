#pragma once
#include "Edge.h"

template <typename Type>
class QueueEdge
{
private:
	Edge<Type>* Heap;
	int hpos;

public:
	QueueEdge(int n);
	~QueueEdge();
	Edge<Type> front();
	void push(Edge<Type> e);
	void pop();
};

#include "QueueEdge.tpp"