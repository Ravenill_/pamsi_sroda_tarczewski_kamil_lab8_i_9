#include "Prim.h"

int main()
{
	GraphArray<int> graf(10, 14);
	GraphArray<int> grafd(10, 9);

	GraphList<int> graf1(4, 4);
	GraphList<int> graf1d(4, 3);

	graf.insertVertex(3);
	graf.insertVertex(6);
	graf.insertVertex(8);
	graf.insertVertex(2);
	graf.insertVertex(33);
	graf.insertVertex(34);
	graf.insertVertex(36);
	graf.insertVertex(37);
	graf.insertVertex(38);
	graf.insertVertex(30);

	graf1.insertVertex(3);
	graf1.insertVertex(6);
	graf1.insertVertex(8);
	graf1.insertVertex(2);

	graf.insertEdge(0, 1, 4);
	graf.insertEdge(1, 2, 3);
	graf.insertEdge(2, 3, 2);
	graf.insertEdge(0, 0, 1);
	graf.insertEdge(0, 1, 5);
	graf.insertEdge(0, 2, 7);
	graf.insertEdge(0, 3, 16);
	graf.insertEdge(0, 4, 14);
	graf.insertEdge(0, 5, 2);
	graf.insertEdge(5, 6, 4);
	graf.insertEdge(4, 7, 7);
	graf.insertEdge(5, 8, 8);
	graf.insertEdge(8, 9, 5);
	graf.insertEdge(6, 7, 43);

	graf1.insertEdge(0, 1, 4);
	graf1.insertEdge(1, 2, 3);
	graf1.insertEdge(2, 3, 2);
	graf1.insertEdge(3, 0, 1);

	for (int i = 0; i < graf.amountOfEdg(); i++)
	{
		std::cout << (*graf.getEdge(i).getA()).getData() << "\n";
		std::cout << (*graf.getEdge(i).getB()).getData() << "\n";
		std::cout << graf.getEdge(i).getData() << "\n";
		std::cout << "\n";
	}
	//system("PAUSE");

	graf.print();
	std::cout << "\n";
	graf.vertices();
	graf.edges();
	//system("PAUSE");

	PrimAlghoritm(graf, grafd);

	grafd.print();
	std::cout << "\n";
	grafd.vertices();
	grafd.edges();
	system("PAUSE");


	graf1.print();
	std::cout << "\n";
	graf1.vertices();
	graf1.edges();

	for (int i = 0; i < graf1.amountOfEdg(); i++)
	{
		std::cout << (*graf1.getEdge(i).getA()).getData() << "\n";
		std::cout << (*graf1.getEdge(i).getB()).getData() << "\n";
		std::cout << graf1.getEdge(i).getData() << "\n";
		std::cout << "\n";
	}

	PrimAlghoritm(graf1, graf1d);

	graf1d.print();
	std::cout << "\n";
	graf1d.vertices();
	graf1d.edges();

	system("PAUSE");
}