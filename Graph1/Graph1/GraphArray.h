#pragma once
#include "Edge.h"
#include "dList.h"

template <typename Type>
class GraphArray
{
private:
	Vertex<Type>* _vertex;
	Edge<Type>* _edge;
	int edg;
	int ver;
	int vertex_id;
	int edge_id;

	Edge<Type>*** matrix;

	Vertex<Type> searchVertex(int id);

public:
	GraphArray();
	GraphArray(int v, int e);
	~GraphArray();

	Vertex<Type>* endVerticles(Edge<Type> edge);
	Vertex<Type> opposite(Vertex<Type> vertex, Vertex<Type> middle);
	bool areAdjacent(Vertex<Type> vertex1, Vertex<Type> vertex2);
	void replace(Vertex<Type> vertex1, Vertex<Type> vertex2);
	void replace(Edge<Type> edge1, Edge<Type> edge2);

	void insertVertex(Type obj);
	void insertEdge(Vertex<Type> vertex1, Vertex<Type> vertex2, Type obj);
	void insertEdge(int vertex1_id, int vertex2_id, Type obj);
	void removeVertex(Vertex<Type> vertex);
	void removeEdge(Edge<Type> edge);

	void incidentEdges(Vertex<Type> vertex);
	void vertices();
	void edges();

	void print();
	void makeMeGraphGizz(int ro);
};

#include "GraphArray.tpp"