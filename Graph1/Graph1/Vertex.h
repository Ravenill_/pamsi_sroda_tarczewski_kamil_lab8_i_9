#pragma once

template <typename TypeData>
class Vertex
{
private:
	int _key;
	TypeData _data;

public:
	Vertex() {};
	~Vertex() {};

	void setKey(TypeData key) { _key = key; }
	int getKey() { return _key; }
	void setData(TypeData data) { _data = data; }
	TypeData getData() { return _data; }
};