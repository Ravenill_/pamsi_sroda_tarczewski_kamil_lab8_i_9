//generator tablic
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>

#define SIZE 10

int main()
{
	int rozmiar1 = floor((0.25 * SIZE * (SIZE - 1)) / 2);
	int rozmiar2 = floor((0.5 * SIZE * (SIZE - 1)) / 2);
	int rozmiar3 = floor((0.75 * SIZE * (SIZE - 1)) / 2);
	int rozmiar4 = floor((1 * SIZE * (SIZE - 1)) / 2);

	int when = 0;

	srand(static_cast<unsigned int>(time(NULL)));

	std::string nazwa1 = std::to_string(SIZE) + "_d25.txt";
	std::string nazwa2 = std::to_string(SIZE) + "_d50.txt";
	std::string nazwa3 = std::to_string(SIZE) + "_d75.txt";
	std::string nazwa4 = std::to_string(SIZE) + "_d100.txt";

	std::fstream file1;
	std::fstream file2;
	std::fstream file3;
	std::fstream file4;

	file1.open(nazwa1.c_str(), std::ios::out);
	file2.open(nazwa2.c_str(), std::ios::out);
	file3.open(nazwa3.c_str(), std::ios::out);
	file4.open(nazwa4.c_str(), std::ios::out);


	file1 << SIZE << "\n" << rozmiar1 << "\n";
	for (int i = 0; i < SIZE; i++)
		file1 << rand() % (SIZE * 10) << "\n";
	when = 0;
	for (int i = 0; i < SIZE; i++)
	{
		if (when >= rozmiar1)
			break;
		for (int j = i+1; j < SIZE; j++)
		{
			if (when >= rozmiar1)
				break;
			file1 << rand() % (SIZE * 10) << " " << i << " " << j << "\n";
			when++;
		}
	}
	std::cout << "Wygenerowano: " << rozmiar1 << "\n";

	file2 << SIZE << "\n" << rozmiar2 << "\n";
	for (int i = 0; i < SIZE; i++)
		file2 << rand() % (SIZE * 10) << "\n";
	when = 0;
	for (int i = 0; i < SIZE; i++)
	{
		if (when >= rozmiar2)
			break;
		for (int j = i + 1; j < SIZE; j++)
		{
			if (when >= rozmiar2)
				break;
			file2 << rand() % (SIZE * 10) << " " << i << " " << j << "\n";
			when++;
		}
	}
	std::cout << "Wygenerowano: " << rozmiar2 << "\n";

	file3 << SIZE << "\n" << rozmiar3 << "\n";
	for (int i = 0; i < SIZE; i++)
		file3 << rand() % (SIZE * 10) << "\n";
	when = 0;
	for (int i = 0; i < SIZE; i++)
	{
		if (when >= rozmiar3)
			break;
		for (int j = i + 1; j < SIZE; j++)
		{
			if (when >= rozmiar3)
				break;
			file3 << rand() % (SIZE * 10) << " " << i << " " << j << "\n";
			when++;
		}
	}
	std::cout << "Wygenerowano: " << rozmiar3 << "\n";

	file4 << SIZE << "\n" << rozmiar4 << "\n";
	for (int i = 0; i < SIZE; i++)
		file4 << rand() % (SIZE * 10) << "\n";
	when = 0;
	for (int i = 0; i < SIZE; i++)
	{
		if (when >= rozmiar4)
			break;
		for (int j = i + 1; j < SIZE; j++)
		{
			if (when >= rozmiar4)
				break;
			file4 << rand() % (SIZE * 10) << " " << i << " " << j << "\n";
			when++;
		}
	}
	std::cout << "Wygenerowano: " << rozmiar4 << "\n";

	file1.close();
	file2.close();
	file3.close();
	file4.close();
	system("PAUSE");
}