#include "QueueEdge.h"
#include "GraphHelper.h"

template<typename Type>
void KruskalAlghoritm(GraphArray<Type>& G, GraphArray<Type>& temp)
{
	//pomocnicze
	int amount_ver = G.amountOfVer();
	int amount_edg = G.amountOfEdg();
	//GraphArray<Type> temp(amount_ver, amount_ver - 1);

	QueueEdge<Type> queue(amount_edg);
	GraphHelper<Type> helper(amount_ver);

	//dodaj do noweg owszytskie wierzcho�ki
	for (int i = 0; i < amount_ver; i++)
		temp.insertVertex(G.getVertex(i));

	//wierzcho�ki do poddrzew (stw�rz nowe poddrzewa - ka�dy z jednym wierzcho�kiem)
	for (int i = 0; i < amount_ver; i++)
		helper.MakeSet(i);

	//pushuj wszytskie kraw�dzie
	for (int i = 0; i < amount_edg; i++)
		queue.push(G.getEdge(i));

	//mainloop
	for (int i = 1; i < amount_ver; i++)
	{
		Edge<Type>* temp_edge;
		do//bierz pierwszy, je�li nie ma cyklu lec dalej
		{
			temp_edge = &queue.front();              
			queue.pop();                   
		} while (helper.FindSet((*(*temp_edge).getA()).getKey()) == helper.FindSet((*(*temp_edge).getB()).getKey()));

		//jesli bangla dodaj do drzewka
		temp.insertEdge(*temp_edge);     
		helper.UnionSets(*temp_edge);          
	}

	//return temp;
}

template<typename Type>
void KruskalAlghoritm(GraphList<Type>& G, GraphList<Type>& temp)
{
	int amount_ver = G.amountOfVer();
	int amount_edg = G.amountOfEdg();
	//GraphArray<Type> temp(amount_ver, amount_ver - 1);

	QueueEdge<Type> queue(amount_edg);
	GraphHelper<Type> helper(amount_ver);

	//dodaj do noweg owszytskie wierzcho�ki
	for (int i = 0; i < amount_ver; i++)
		temp.insertVertex(G.getVertex(i));

	//wierzcho�ki do poddrzew (stw�rz nowe poddrzewa - ka�dy z jednym wierzcho�kiem)
	for (int i = 0; i < amount_ver; i++)
		helper.MakeSet(i);

	//pushuj wszytskie kraw�dzie
	for (int i = 0; i < amount_edg; i++)
		queue.push(G.getEdge(i));

	//mainloop
	for (int i = 1; i < amount_ver; i++)
	{
		Edge<Type>* temp_edge;
		do//bierz pierwszy, je�li nie ma cyklu lec dalej
		{
			temp_edge = &queue.front();          
			queue.pop();                  
		} while (helper.FindSet((*(*temp_edge).getA()).getKey()) == helper.FindSet((*(*temp_edge).getB()).getKey()));

		//jesli bangla dodaj do drzewka
		temp.insertEdge(*temp_edge);       
		helper.UnionSets(*temp_edge);            
	}

	//return temp;
}