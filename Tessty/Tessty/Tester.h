#pragma once
#include <fstream>
#include <string>

class Tester
{
private:
	int size;

	std::fstream filein;
	std::string nazwain;

public:
	Tester(int a);

	double kruskala(const char *);
	double kruskall(const char *);
	double prima(const char *);
	double priml(const char *);
};