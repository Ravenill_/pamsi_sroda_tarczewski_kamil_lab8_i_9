#pragma once
#include "GraphArray.h"
#include "GraphList.h"

template <typename Type>
void PrimAlghoritm(GraphArray<Type>& G, GraphArray<Type>& temp);

template <typename Type>
void PrimAlghoritm(GraphList<Type>& G, GraphList<Type>& temp);

#include "Prim.tpp"