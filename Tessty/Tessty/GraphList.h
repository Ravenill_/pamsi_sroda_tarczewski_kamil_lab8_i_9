#pragma once
#include "Edge.h"
#include "dList.h"

template <typename Type>
class GraphList
{
private:
	Vertex<Type>* _vertex;
	Edge<Type>* _edge;
	int edg;
	int ver;
	int vertex_id;
	int edge_id;

	dList<Edge<Type>*>* list;

	Vertex<Type> searchVertex(int id);

public:
	GraphList(int v, int e);
	~GraphList();

	void insertVertex(Type obj);
	void insertVertex(Vertex<Type> vertex);
	void insertEdge(Vertex<Type>* vertex1, Vertex<Type>* vertex2, Type obj);
	void insertEdge(int vertex1_id, int vertex2_id, Type obj);
	void insertEdge(Edge<Type> e);

	void vertices();
	void edges();

	void print();

	int amountOfVer() { return ver; }
	int amountOfEdg() { return edg; }

	Vertex<Type> getVertex(int i) { return _vertex[i]; }
	Edge<Type> getEdge(int i) { return _edge[i]; }
	dList<Edge<Type>*> getList(int i) { return list[i]; }
};

#include "GraphList.tpp"