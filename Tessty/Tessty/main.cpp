#include "Tester.h"
#include <iostream>

#define IT 100

int main()
{
	int SIZE[5] = { 10, 50, 100, 500, 1000 };
	const char * RO[4] = { "_d25", "_d50", "_d75", "_d100" };
	double medium;
	std::fstream medi;

	for (int s = 0; s < 5; s++)
	{
		std::string nan = "medium_time_" + std::to_string(SIZE[s]) + ".txt";
		medi.open(nan.c_str(), std::ios::out);
		for (int j = 0; j < 4; j++)
		{
			Tester tester(SIZE[s]);
			double time;

			std::fstream fileout1;
			std::fstream fileout2;
			std::fstream fileout3;
			std::fstream fileout4;
			std::string nazwaout1 = "time_" + std::to_string(SIZE[s]) + RO[j] + "_Kruskal_Array" + ".txt";
			std::string nazwaout2 = "time_" + std::to_string(SIZE[s]) + RO[j] + "_Kruskal_List" + ".txt";
			std::string nazwaout3 = "time_" + std::to_string(SIZE[s]) + RO[j] + "_Prim_Array" + ".txt";
			std::string nazwaout4 = "time_" + std::to_string(SIZE[s]) + RO[j] + "_Prim_List" + ".txt";

			fileout1.open(nazwaout1.c_str(), std::ios::out);

			medium = 0;
			for (int i = 1; i <= IT; i++)
			{
				time = tester.kruskala(RO[j]);
				medium += time;
				fileout1 << time << "\n";
				std::cout << "Kruskal Array, Graf " << i << ", rozmiar: " << SIZE[s] << ", wypełnienie: " << RO[j] << "%, czas: " << time << "\n";
			}
			medium = medium / IT;
			medi << medium << "\n";
			fileout1.close();
			std::cout << "\n";

			fileout2.open(nazwaout2.c_str(), std::ios::out);

			medium = 0;
			for (int i = 1; i <= IT; i++)
			{
				time = tester.kruskall(RO[j]);
				medium += time;
				fileout2 << time << "\n";
				std::cout << "Kruskal List, Graf " << i << ", rozmiar: " << SIZE[s] << ", wypełnienie: " << RO[j] << "%, czas: " << time << "\n";
			}
			medium = medium / IT;
			medi << medium << "\n";
			fileout2.close();
			std::cout << "\n";
			fileout3.open(nazwaout3.c_str(), std::ios::out);

			medium = 0;
			for (int i = 1; i <= IT; i++)
			{
				time = tester.prima(RO[j]);
				medium += time;
				fileout3 << time << "\n";
				std::cout << "Prim Array, Graf " << i << ", rozmiar: " << SIZE[s] << ", wypełnienie: " << RO[j] << "%, czas: " << time << "\n";
			}
			medium = medium / IT;
			medi << medium << "\n";
			fileout3.close();
			std::cout << "\n";

			fileout4.open(nazwaout4.c_str(), std::ios::out);

			medium = 0;
			for (int i = 1; i <= IT; i++)
			{
				time = tester.priml(RO[j]);
				medium += time;
				fileout4 << time << "\n";
				std::cout << "Prim List, Graf " << i << ", rozmiar: " << SIZE[s] << ", wypełnienie: " << RO[j] << "%, czas: " << time << "\n";
			}
			medium = medium / IT;
			medi << medium << "\n";
			fileout4.close();
			std::cout << "\n";
		}
		medi.close();
		std::cout << "\n__________________\n";
	}
	system("PAUSE");
}