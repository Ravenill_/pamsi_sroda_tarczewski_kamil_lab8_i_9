#include "Tester.h"
#include "Timer.h"
#include "Kruskal.h"
#include "Prim.h"
#include <cstdlib>
#include <iostream>

Tester::Tester(int a)
{
	size = a;
}

double Tester::kruskala(const char * ro)
{
	Timer clock;
	double t1, t2;
	int v, e;
	int temp, temp2, temp3;
	GraphArray<int> *graph1;
	GraphArray<int> *graph2;

	nazwain = std::to_string(size) + ro + ".txt";
	filein.open(nazwain.c_str(), std::ios::in);
	
	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		system("PAUSE");
		return 0;
	}

	filein >> v;
	filein >> e;

	graph1 = new GraphArray<int>(v, e);
	graph2 = new GraphArray<int>(v, v-1);

	for (int i = 0; i < v; i++)
	{
		filein >> temp;
		(*graph1).insertVertex(temp);
	}
	for (int i = 0; i < e; i++)
	{
		filein >> temp;
		filein >> temp2;
		filein >> temp3;
		(*graph1).insertEdge(temp2, temp3, temp);
	}

	filein.close();

	clock.StartCounter();
	t1 = clock.GetCounter();
	KruskalAlghoritm(*graph1, *graph2);
	t2 = clock.GetCounter();

	delete graph1;
	delete graph2;
	return t2 - t1;
}

double Tester::kruskall(const char * ro)
{
	Timer clock;
	double t1, t2;
	int v, e;
	int temp, temp2, temp3;
	GraphList<int> *graph1;
	GraphList<int> *graph2;

	nazwain = std::to_string(size) + ro + ".txt";
	filein.open(nazwain.c_str(), std::ios::in);

	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		system("PAUSE");
		return 0;
	}

	filein >> v;
	filein >> e;

	graph1 = new GraphList<int>(v, e);
	graph2 = new GraphList<int>(v, v - 1);

	for (int i = 0; i < v; i++)
	{
		filein >> temp;
		(*graph1).insertVertex(temp);
	}
	for (int i = 0; i < e; i++)
	{
		filein >> temp;
		filein >> temp2;
		filein >> temp3;
		(*graph1).insertEdge(temp2, temp3, temp);
	}

	filein.close();

	clock.StartCounter();
	t1 = clock.GetCounter();
	KruskalAlghoritm(*graph1, *graph2);
	t2 = clock.GetCounter();

	delete graph1;
	delete graph2;
	return t2 - t1;
}

double Tester::prima(const char * ro)
{
	Timer clock;
	double t1, t2;
	int v, e;
	int temp, temp2, temp3;
	GraphArray<int> *graph1;
	GraphArray<int> *graph2;

	nazwain = std::to_string(size) + ro + ".txt";
	filein.open(nazwain.c_str(), std::ios::in);

	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		system("PAUSE");
		return 0;
	}

	filein >> v;
	filein >> e;

	graph1 = new GraphArray<int>(v, e);
	graph2 = new GraphArray<int>(v, v - 1);

	for (int i = 0; i < v; i++)
	{
		filein >> temp;
		(*graph1).insertVertex(temp);
	}
	for (int i = 0; i < e; i++)
	{
		filein >> temp;
		filein >> temp2;
		filein >> temp3;
		(*graph1).insertEdge(temp2, temp3, temp);
	}

	filein.close();

	clock.StartCounter();
	t1 = clock.GetCounter();
	PrimAlghoritm(*graph1, *graph2);
	t2 = clock.GetCounter();

	delete graph1;
	delete graph2;
	return t2 - t1;
}

double Tester::priml(const char * ro)
{
	Timer clock;
	double t1, t2;
	int v, e;
	int temp, temp2, temp3;
	GraphList<int> *graph1;
	GraphList<int> *graph2;

	nazwain = std::to_string(size) + ro + ".txt";
	filein.open(nazwain.c_str(), std::ios::in);

	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		system("PAUSE");
		return 0;
	}

	filein >> v;
	filein >> e;

	graph1 = new GraphList<int>(v, e);
	graph2 = new GraphList<int>(v, v - 1);

	for (int i = 0; i < v; i++)
	{
		filein >> temp;
		(*graph1).insertVertex(temp);
	}
	for (int i = 0; i < e; i++)
	{
		filein >> temp;
		filein >> temp2;
		filein >> temp3;
		(*graph1).insertEdge(temp2, temp3, temp);
	}

	filein.close();

	clock.StartCounter();
	t1 = clock.GetCounter();
	PrimAlghoritm(*graph1, *graph2);
	t2 = clock.GetCounter();

	delete graph1;
	delete graph2;
	return t2 - t1;
}