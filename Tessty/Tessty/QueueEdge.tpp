
template <typename Type>
QueueEdge<Type>::QueueEdge(int n)
{
	Heap = new Edge<Type>[n];           
	hpos = 0;                     
}

template <typename Type>
QueueEdge<Type>::~QueueEdge()
{
	delete[] Heap;
}

template <typename Type>
Edge<Type> QueueEdge<Type>::front()
{
	return Heap[0];
}

//pushnięcie na stos
template <typename Type>
void QueueEdge<Type>::push(Edge<Type> e)
{
	int i, j;

	i = hpos++;                 
	j = (i - 1) >> 1;        //przesunięcie o bit w prawo (dzielnie na 2)    

	while (i && (Heap[j].getData() > e.getData()))
	{
		Heap[i] = Heap[j];
		i = j;
		j = (i - 1) >> 1;
	}
	Heap[i] = e;               
}

//zpushowanie z kopca
template <typename Type>
void QueueEdge<Type>::pop()
{
	int i, j;
	Edge<Type> e;

	if (hpos)
	{
		e = Heap[--hpos];
		i = 0;
		j = 1;

		while (j < hpos)
		{
			if ((j + 1 < hpos) && (Heap[j + 1].getData() < Heap[j].getData())) j++;
			if (e.getData() <= Heap[j].getData()) break;
			Heap[i] = Heap[j];
			i = j;
			j = (j << 1) + 1;
		}
		Heap[i] = e;
	}
}