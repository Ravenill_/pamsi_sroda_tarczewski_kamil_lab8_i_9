#include <iostream>

template<typename Type>
GraphArray<Type>::GraphArray(int v, int e)
{
	edg = e;
	ver = v;
	vertex_id = 0;
	edge_id = 0;

	matrix = new Edge<Type>**[ver];
	for (int i = 0; i < ver; i++)
		matrix[i] = new Edge<Type>*[ver];

	for (int i = 0; i < ver; i++)
		for (int j = 0; j < ver; j++)
			matrix[i][j] = nullptr;

	_vertex = new Vertex<Type> [ver];
	_edge = new Edge<Type> [edg];
}

template<typename Type>
GraphArray<Type>::~GraphArray()
{
	for (int i = 0; i < ver; i++)
		delete[] matrix[i];

	delete[] matrix;
	delete[] _edge;
	delete[] _vertex;
}

template<typename Type>
void GraphArray<Type>::insertVertex(Type obj)
{
	if (vertex_id <= ver)
	{
		Vertex<Type> *temp = new Vertex<Type>;
		(*temp).setData(obj);
		(*temp).setKey(vertex_id);

		_vertex[vertex_id++] = *temp;
	}
}

template<typename Type>
void GraphArray<Type>::insertVertex(Vertex<Type> vertex)
{
	if (vertex_id <= ver)
		_vertex[vertex_id++] = vertex;
}

//insertnij krew�d� poprzez wierzcho�ki
template<typename Type>
void GraphArray<Type>::insertEdge(Vertex<Type>* vertex1, Vertex<Type>* vertex2, Type obj)
{
	Edge<Type> *temp = new Edge<Type>;
	(*temp).setA(vertex1);
	(*temp).setB(vertex2);
	(*temp).setData(obj);

	//je�li nie ma po��czenia
	if (matrix[(*vertex1).getKey()][(*vertex2).getKey()] == nullptr && matrix[(*vertex2).getKey()][(*vertex1).getKey()] == nullptr)
	{
		_edge[edge_id++] = *temp;
		matrix[(*vertex1).getKey()][(*vertex2).getKey()] = temp;
		matrix[(*vertex2).getKey()][(*vertex1).getKey()] = temp;
	}
	else
		delete temp;
}

//dodaj kraw�d� przez krew�d�
template<typename Type>
void GraphArray<Type>::insertEdge(Edge<Type> e)
{
	//jesli nie ma po��czenia
	if (matrix[(*e.getA()).getKey()][(*e.getB()).getKey()] == nullptr && matrix[(*e.getB()).getKey()][(*e.getA()).getKey()] == nullptr)
	{
		_edge[edge_id++] = e;
		matrix[(*e.getA()).getKey()][(*e.getB()).getKey()] = &e;
		matrix[(*e.getB()).getKey()][(*e.getA()).getKey()] = &e;
	}
}

//poka wierzcho�ki
template<typename Type>
void GraphArray<Type>::vertices()
{
	for (int i = 0; i < ver; i++)
		std::cout << _vertex[i].getData() << ":" << _vertex[i].getKey() <<  "  ";
	std::cout << "\n";
}

//poka edgesy
template<typename Type>
void GraphArray<Type>::edges()
{
	for (int i = 0; i < edg; i++)
		std::cout << _edge[i].getData() << "  ";
	std::cout << "\n";
}

//wyprintuj macierz
template<typename Type>
void GraphArray<Type>::print()
{
	for (int i = 0; i < ver; i++)
	{
		for (int j = 0; j < ver; j++)
			std::cout << matrix[i][j] << "  ";
		std::cout << "\n";
	}
}

//wyszukaj wierzcho�ek o danym id
template<typename Type>
Vertex<Type> GraphArray<Type>::searchVertex(int id)
{
	for (int i = 0; i < ver; i++)
	{
		if (_vertex[i].getKey() == id)
			return _vertex[i];
	}
	return Vertex<Type>();
}
//w�� przez id
template<typename Type>
void GraphArray<Type>::insertEdge(int vertex1_id, int vertex2_id, Type obj)
{
	insertEdge(&_vertex[vertex1_id], &_vertex[vertex2_id], obj);
}

