#pragma once
#include "Vertex.h"

template <typename Type>
class Edge
{
private:
	Vertex<Type> *_a, *_b;
	Type _data;

public:
	Edge() {};
	~Edge() {};

	void setData(Type data) { _data = data; }
	Type getData() { return _data; }
	void setA(Vertex<Type>* a) { _a = a; }
	Type getA() { return _a; }
	void setB(Vertex<Type>* b) { _b = b; }
	Type getB() { return _b; }
};