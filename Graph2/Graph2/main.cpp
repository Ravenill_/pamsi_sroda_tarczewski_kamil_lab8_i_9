#include "GraphList.h"
#include <cstdlib>

int main()
{
	GraphList<int> a(4,2);
	
	a.insertVertex(3);
	a.insertVertex(5);
	a.insertVertex(7);
	a.insertVertex(12);

	a.insertEdge(0, 1, 10);
	a.insertEdge(1, 2, 3);

	a.vertices();
	std::cout << "\n";
	a.edges();
	std::cout << "\n";
	a.print();
	system("PAUSE");
}