#pragma once

template <typename Type>
class Vertex
{
private:
	int _key;
	Type _data;

public:
	Vertex(): _key(0) {}
	~Vertex() {}

	void setKey(Type key) { _key = key; }
	int getKey() { return _key; }
	void setData(Type data) { _data = data; }
	Type getData() { return _data; }
	void insertToList(int edge) { (*list).get().wstawPierwszy(edge); }
	int getFromList(int i) { return (*list).get().wezDane(i); }
};