#pragma once
#include "Edge.h"
// Definicja obiektu struktury zbior�w roz��cznych
//------------------------------------------------
struct DSNode
{
	int up, rank;
};

template <typename Type>
class DSStruct
{
private:
	DSNode * Z;

public:
	DSStruct(int n);
	~DSStruct();
	void MakeSet(int v);
	int FindSet(int v);
	void UnionSets(Edge<Type> e);
};

#include "GraphHelper.tpp"