
// Konstruktor
template <typename Type>
DSStruct<Type>::DSStruct(int n)
{
	Z = new DSNode[n];             // Tworzymy tablic� dla element�w zbior�w
}

// Destruktor
//-----------
template <typename Type>
DSStruct<Type>::~DSStruct()
{
	delete[] Z;                    // Usuwamy tablic� ze zbiorami
}

template <typename Type>
void DSStruct<Type>::MakeSet(int v)
{
	Z[v].up = v;
	Z[v].rank = 0;
}

// Zwraca indeks reprezentanta zbioru, w kt�rym jest wierzcho�ek v
//----------------------------------------------------------------
template <typename Type>
int DSStruct<Type>::FindSet(int v)
{
	if (Z[v].up != v)
		Z[v].up = FindSet(Z[v].up);
	return Z[v].up;
}

// ��czy ze sob� zbiory z v i u
//-----------------------------
template <typename Type>
void DSStruct<Type>::UnionSets(Edge<Type> e)
{
	int ru, rv;

	ru = FindSet(e.getA().getKey());             // Wyznaczamy korze� drzewa z w�z�em u
	rv = FindSet(e.getB().getKey());             // Wyznaczamy korze� drzewa z w�z�em v
	if (ru != rv)                    // Korzenie musz� by� r�ne
	{
		if (Z[ru].rank > Z[rv].rank)   // Por�wnujemy rangi drzew
			Z[rv].up = ru;              // ru wi�ksze, do��czamy rv
		else
		{
			Z[ru].up = rv;              // r�wne lub rv wi�ksze, do��czamy ru
			if (Z[ru].rank == Z[rv].rank) 
				Z[rv].rank++;
		}
	}
}