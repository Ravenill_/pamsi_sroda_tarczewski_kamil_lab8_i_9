#pragma once
#include <iostream>

template <typename Type>
class mNode
{
protected:
	Type data;
	mNode<Type> *next;
	mNode<Type> *prev;

public:
	mNode() :next(NULL), prev(NULL) {};
	~mNode() {};

	//metody m�wi� same za siebie - nawet je�li g�osu nie maj�
	mNode<Type>* getNext() { return next; }
	void setNext(mNode<Type> *a) { next = a; }
	mNode<Type>* getPrev() { return prev; }
	void setPrev(mNode<Type> *a) { prev = a; }
	Type getData() { return data; }
	void setData(Type a) { data = a; }
};