
template <typename Type>
QueueEdge<Type>::QueueEdge(int n)
{
	Heap = new Edge<Type>[n];            // Tworzymy tablic�
	hpos = 0;                       // Pozycja w kopcu
}

// Destruktor - usuwa kopiec z pami�ci
//------------------------------------
template <typename Type>
QueueEdge<Type>::~QueueEdge()
{
	delete[] Heap;
}

// Zwraca kraw�d� z pocz�tku kopca
//--------------------------------
template <typename Type>
Edge<Type> QueueEdge<Type>::front()
{
	return Heap[0];
}

// Umieszcza w kopcu now� kraw�d� i odtwarza struktur� kopca
//----------------------------------------------------------
template <typename Type>
void QueueEdge<Type>::push(Edge<Type> e)
{
	int i, j;

	i = hpos++;                     // i ustawiamy na koniec kopca
	j = (i - 1) >> 1;               // Obliczamy pozycj� rodzica
									// Szukamy miejsca w kopcu dla e

	while (i && (Heap[j].getData() > e.getData()))
	{
		Heap[i] = Heap[j];
		i = j;
		j = (i - 1) >> 1;
	}
	Heap[i] = e;                    // Kraw�d� e wstawiamy do kopca
}

// Usuwa korze� z kopca i odtwarza jego struktur�
//-----------------------------------------------
template <typename Type>
void QueueEdge<Type>::pop()
{
	int i, j;
	Edge<Type> e;

	if (hpos)
	{
		e = Heap[--hpos];

		i = 0;
		j = 1;

		while (j < hpos)
		{
			if ((j + 1 < hpos) && (Heap[j + 1].getData() < Heap[j].getData())) j++;
			if (e.getData() <= Heap[j].getData()) break;
			Heap[i] = Heap[j];
			i = j;
			j = (j << 1) + 1;
		}
		Heap[i] = e;
	}
}