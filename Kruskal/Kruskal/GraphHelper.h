#pragma once
#include "Edge.h"
// Definicja obiektu struktury zbior�w roz��cznych
//------------------------------------------------
struct DSNode
{
	int up, rank;
};

template <typename Type>
class GraphHelper
{
private:
	DSNode * Z;

public:
	GraphHelper(int n);
	~GraphHelper();
	void MakeSet(int v);
	int FindSet(int v);
	void UnionSets(Edge<Type> e);
};

#include "GraphHelper.tpp"