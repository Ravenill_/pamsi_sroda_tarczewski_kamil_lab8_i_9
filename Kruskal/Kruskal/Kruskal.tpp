#include "QueueEdge.h"
#include "GraphHelper.h"

template<typename Type>
void KruskalAlghoritm(GraphArray<Type>& G, GraphArray<Type>& temp)
{
	int amount_ver = G.amountOfVer();
	int amount_edg = G.amountOfEdg();
	//GraphArray<Type> temp(amount_ver, amount_ver - 1);

	QueueEdge<Type> queue(amount_edg);
	GraphHelper<Type> helper(amount_ver);

	for (int i = 0; i < amount_ver; i++)
		temp.insertVertex(G.getVertex(i));

	for (int i = 0; i < amount_ver; i++)
		helper.MakeSet(i);

	for (int i = 0; i < amount_edg; i++)
		queue.push(G.getEdge(i));

	for (int i = 1; i < amount_ver; i++)
	{
		Edge<Type>* temp_edge;
		do
		{
			temp_edge = &queue.front();              
			queue.pop();                   
		} while (helper.FindSet((*(*temp_edge).getA()).getKey()) == helper.FindSet((*(*temp_edge).getB()).getKey()));

		temp.insertEdge(*temp_edge);     
		helper.UnionSets(*temp_edge);          
	}

	//return temp;
}

template<typename Type>
void KruskalAlghoritm(GraphList<Type>& G, GraphList<Type>& temp)
{
	int amount_ver = G.amountOfVer();
	int amount_edg = G.amountOfEdg();
	//GraphArray<Type> temp(amount_ver, amount_ver - 1);

	QueueEdge<Type> queue(amount_edg);
	GraphHelper<Type> helper(amount_ver);

	for (int i = 0; i < amount_ver; i++)
		temp.insertVertex(G.getVertex(i));

	for (int i = 0; i < amount_ver; i++)
		helper.MakeSet(i);

	for (int i = 0; i < amount_edg; i++)
		queue.push(G.getEdge(i));

	for (int i = 1; i < amount_ver; i++)
	{
		Edge<Type>* temp_edge;
		do
		{
			temp_edge = &queue.front();          
			queue.pop();                  
		} while (helper.FindSet((*(*temp_edge).getA()).getKey()) == helper.FindSet((*(*temp_edge).getB()).getKey()));

		temp.insertEdge(*temp_edge);       
		helper.UnionSets(*temp_edge);            
	}

	//return temp;
}