#pragma once
#include "GraphArray.h"
#include "GraphList.h"

template <typename Type>
void KruskalAlghoritm(GraphArray<Type>& G, GraphArray<Type>& temp);

template <typename Type>
void KruskalAlghoritm(GraphList<Type>& G, GraphList<Type>& temp);

#include "Kruskal.tpp"
