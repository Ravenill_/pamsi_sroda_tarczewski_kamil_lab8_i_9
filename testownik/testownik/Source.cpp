#include <iostream>
#include <fstream>
#include <string>

int main()
{
	int v, e, temp, temp2, temp3;
	
	std::string nazwain = "10_d25.txt";
	std::fstream filein;
	filein.open(nazwain.c_str(), std::ios::in);

	if (!filein.good())
	{
		std::cerr << "Nie otworzono pliku: " << nazwain << "\n";
		system("PAUSE");
		return 0;
	}

	filein >> v;
	filein >> e;
	std::cout << v << "\n" << e << "\n";

	for (int i = 0; i < v; i++)
	{
		filein >> temp;
		std::cout << temp << "\n";
	}
	for (int i = 0; i < e; i++)
	{
		filein >> temp;
		filein >> temp2;
		filein >> temp3;
		std::cout << temp << " " << temp2 << " " << temp3 << "\n";
	}

	filein.close();
	system("PAUSE");
}